package com.team8.locky

import android.app.*
import android.os.Bundle

import android.view.View
import android.widget.*
import java.text.SimpleDateFormat
import java.util.*

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.design.widget.CollapsingToolbarLayout
import android.util.Log
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.team8.locky.Models.GSON.ReservaCrear
import com.team8.locky.Models.data.ApiLockyService
import kotlinx.android.synthetic.main.activity_reservation.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception



class ReservationActivity : BaseActivity() {

    //Arreglos para los spinner
    var sizes = arrayOf("Pequeño", "Mediano", "Grande")
    //var locations =arrayOf("Alto", "Medio", "Bajo")
    var tarifas = arrayOf("$1.000", "$2.000", "$3.000")


    lateinit var userEmailSaved: String
    var prefs: SharedPreferences? = null
    var textview_date: TextView? = null
    var cal = Calendar.getInstance()
    var textview_in: TextView? = null
    var textview_out: TextView? = null
    lateinit var textview_tarifa : TextView
    lateinit var tiempoInicio: String
    lateinit var tiempoFin: String
    var lugar: String = "Parque de la 93"

    //Info del locker
    lateinit var tamanioLocker : String
    //Alarma
    lateinit var context: Context
    lateinit var alarmManager: AlarmManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservation)
        prefs = this.getSharedPreferences("com.team8.locky.prefs", 0)
        userEmailSaved = prefs!!.getString("user_email", "")
        Picasso.get().load("https://bit.ly/2CFgvLU").into(toolbarImage)
        val intent = intent
        lugar = intent.getStringExtra("id_place")
        println("HAS INGRESADO A: "+lugar)
        val collapsingToolbarLayout = findViewById<CollapsingToolbarLayout>(R.id.collapsingToolbar)
        collapsingToolbarLayout.setTitle(lugar)

        //Asignamos los valores a los spinners
        val size_spinner = findViewById<Spinner>(R.id.size_spinner)
        if (size_spinner != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, sizes)
            size_spinner.adapter = arrayAdapter

            size_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    tamanioLocker = sizes[position]
                    textview_tarifa = findViewById(R.id.textViewTarifa)
                    when (tamanioLocker) {

                        sizes.get(0) -> textview_tarifa.setText(tarifas.get(0))
                        sizes.get(1) -> textview_tarifa.setText(tarifas.get(1))
                        sizes.get(2) -> textview_tarifa.setText(tarifas.get(2))
                        else -> textview_tarifa.setText("$---")
                    }


                }
                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
        }

        // get the references from layout file
        textview_date = findViewById<TextView>(R.id.fecha_inicio_reserva)
        textview_in = findViewById(R.id.hora_inicio_reserva)
        textview_out = findViewById(R.id.hora_fin_reserva)




        // create an OnDateSetListener
        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()

            }
        }

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        textview_date!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                var datepicker = DatePickerDialog(this@ReservationActivity,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH))
                //val today = Date().time
                val calendar = Calendar.getInstance()
                val today = calendar.time

                calendar.add(Calendar.DAY_OF_YEAR, 1)
                val tomorrow = calendar.time
                datepicker.datePicker.minDate = tomorrow.time
                cal.time=Date()

                datepicker.datePicker.maxDate = today.time +31556900000
                datepicker.show()

            }

        })

    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        // etc.
        val fechaReserva = findViewById<TextView>(R.id.fecha_inicio_reserva).text.toString()
        savedInstanceState.putString("fechaReserva", fechaReserva)
        val horaInicioReserva = findViewById<TextView>(R.id.hora_inicio_reserva).text.toString()
        savedInstanceState.putString("horaInicioReserva", horaInicioReserva)
        val horaFinReserva = findViewById<TextView>(R.id.hora_fin_reserva).text.toString()
        savedInstanceState.putString("horaFinReserva", horaFinReserva)
        super.onSaveInstanceState(savedInstanceState)

    }

    public override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        findViewById<TextView>(R.id.fecha_inicio_reserva).text = savedInstanceState.getString("fechaReserva")
        findViewById<TextView>(R.id.hora_inicio_reserva).text = savedInstanceState.getString("horaInicioReserva")
        findViewById<TextView>(R.id.hora_fin_reserva).text = savedInstanceState.getString("horaFinReserva")

    }

    private fun updateDateInView() {
        val myFormat = "yyyy-MM-dd" // formato dia mes año
        val sdf = SimpleDateFormat(myFormat, Locale.UK)
        textview_date!!.setText(sdf.format(cal.getTime()))
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun clickTimePicker(view: View) {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)

        val tpd = TimePickerDialog(this,TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
            var minutos :String
            var horas  :String
            if (h<9) horas="0" else horas =""
            if (m<9) minutos="0" else minutos =""

            textview_in!!.setText(""+horas+ h.toString() + " : "+minutos+ m )
        }),hour,minute,false)


        tpd.show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun changeOutTime(view: View) {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)

        val tpd = TimePickerDialog(this,TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
            var minutos :String
            var horas : String
            if (h<9) horas="0" else horas =""
            if (m<9) minutos="0" else minutos =""
            textview_out!!.setText(""+horas+ h.toString() + " : "+minutos+ m )
        }),hour,minute,false)

        tpd.show()
    }

    fun checkMyLocker(view: View){
        saveReserva()
    }

    private fun saveReserva(){
        val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        if(isConnected){
            val tamanoEscogido =  findViewById<Spinner>(R.id.size_spinner).getSelectedItem().toString()
            val contextAct = this@ReservationActivity
             try{
                    tiempoInicio = findViewById<TextView>(R.id.fecha_inicio_reserva).text.toString() + " " + findViewById<TextView>(R.id.hora_inicio_reserva).text.toString().replace("\\s".toRegex(), "")
                    println(tiempoInicio)
                    val pattern = "yyyy-MM-dd hh:mm"

                    tiempoFin =  findViewById<TextView>(R.id.fecha_inicio_reserva).text.toString() + " " + findViewById<TextView>(R.id.hora_fin_reserva).text.toString().replace("\\s".toRegex(), "")
                    val fecha_fin = SimpleDateFormat(pattern).parse(tiempoFin)

                   val reserva = ReservaCrear(
                        tiempoInicio,
                        tiempoFin,
                        5000,
                        tamanoEscogido,
                         lugar,
                        Integer.parseInt(userEmailSaved)
                    )

                 val activity = this
                 val apiService = ApiLockyService()
                 GlobalScope.launch(Dispatchers.Main) {
                 apiService.crearReserva(reserva).enqueue(object : Callback<ReservaCrear> {
                         override fun onResponse(call: Call<ReservaCrear>, response: Response<ReservaCrear>) {
                             Log.d("Crear Reserva", "" + response)
                             if(response.code()==500){
                                showDialogTryAgain(contextAct)
                             }
                             else if(response.code()==200){
                                 showDialogOK(contextAct, activity)
                             }
                         }

                         override fun onFailure(call: Call<ReservaCrear>, t: Throwable) {
                             println("++++++++++++++++++++++++++++++++")
                         }
                    })
                     context = contextAct
                     alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
                     var second =  fecha_fin.time - System.currentTimeMillis()
                     var minutosAntes =10
                     second = second-(1000*60*minutosAntes)
                     val intent = Intent(context, Receiver::class.java)
                     val pendingIntent = PendingIntent.getBroadcast(context,0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                     alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + second,pendingIntent)

                     Log.d("MainActivity","Create : "+Date().toString())
                 }
                }catch (e: Exception){
                 val builder = AlertDialog.Builder(this@ReservationActivity)
                 // Set the alert dialog title
                 builder.setTitle("No se encontraron casilleros con el filtro seleccionado")
                 // Display a message on alert dialog
                 builder.setMessage("Por favor, intenalo de nuevo")
                 // Finally, make the alert dialog using builder
                 val dialog: AlertDialog = builder.create()
                 // Display the alert dialog on app interface
                 dialog.show()
                 println(e)
                 throw(e)
                }
            }
        else{
            Toast.makeText(this@ReservationActivity, "No tienes conexión a internet, por favor intenta de nuevo más tarde", Toast.LENGTH_SHORT ).show()
        }
    }
    private fun showDialogTryAgain(ctx: Context){
        val builder = AlertDialog.Builder(ctx)
        // Set the alert dialog title
        builder.setTitle("No se encontraron casilleros con el filtro seleccionado")
        // Display a message on alert dialog
        builder.setMessage("Por favor, intenalo de nuevo")
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }
    private fun showDialogOK(ctx: Context, activity: Activity){
        val builder = AlertDialog.Builder(ctx)
        // Set the alert dialog title
        builder.setTitle("Tu reserva ha sido realizada")
        // Display a message on alert dialog
        builder.setMessage("Puedes administrar tus reservas en la sección 'Mis Reservas'")
        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK"){dialog, which ->
            // Do something when user press the positive button
            activity.finish()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()
        // Display the alert dialog on app interface
        dialog.show()
    }
    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
