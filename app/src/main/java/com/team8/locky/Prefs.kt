package com.team8.locky

import android.content.Context
import android.content.SharedPreferences

class Prefs(context: Context) {
    val PREFS_FILENAME = "com.team8.locky.prefs"
    val USER_EMAIL = "user_email"
    var prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)
    var userEmail:String
        get() = prefs!!.getString(USER_EMAIL, "")
        set(value) = prefs.edit().putString(USER_EMAIL, value).apply()
}